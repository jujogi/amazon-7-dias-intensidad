import Vue from "vue";
import VueRouter from "vue-router";

import IndexApp from "../components/App/Start.vue";
import HomeApp from "../components/App/Home.vue";
import Landing from "../components/App/Landing.vue";
import MomentToEat from "../components/App/MomentToEat.vue";
import Tools from "../components/App/Tools.vue";
import Protein from "../components/App/Protein.vue";
import Grain from "../components/App/Grain.vue";
import Vegetal from "../components/App/Vegetal.vue";
import Fruit from "../components/App/Fruit.vue";
import Spice from "../components/App/Spice.vue";
import UserForm from "../components/App/User-form.vue";


// import IndexDash from "../components/Dashboard/Index.vue";
// import HomeDashboard from "../components/Dashboard/Home.vue";
// import Family from "../components/Dashboard/Family.vue";
// import Category from "../components/Dashboard/Category.vue";
// import Cook from "../components/Dashboard/Cook.vue";
// import Tool from "../components/Dashboard/Tools.vue";

import Recipes from "../components/Recipes.vue";
import Recipe from "../components/Recipe.vue";
import SetRecipes from "../components/SetRecipes.vue";

Vue.use(VueRouter);

const routes = [{
  path: "/",
  component: IndexApp,
  children: [{
      path: "",
      component: Landing,
      name: "landing"
    },
    // {
    //   path: "/CRO",
    //   component: Landing,
    //   name: "Landing"
    // },
    {
      path: "/hora-de-comer",
      component: MomentToEat,
      name: "momento"
    },
    {
      path: "/herramientas",
      component: Tools,
      name: "herramientas"
    },
    {
      path: "/proteinas",
      component: Protein,
      name: "proteinas"
    },
    {
      path: "/granos",
      component: Grain,
      name: "granos"
    },
    {
      path: "/vegetales",
      component: Vegetal,
      name: "vegetales"
    },
    {
      path: "/frutas",
      component: Fruit,
      name: "frutas"
    },
    {
      path: "/picante",
      component: Spice,
      name: "picante"
    },
    {
      path: "/formulario",
      component: UserForm,
      name: "formulario"
    },
    {
      path: "/recetas",
      component: Recipes,
      name: "recetas"
    },
    {
      path: "/recetas/:set",
      component: SetRecipes,
      name: "recetasSet",
      props: true
    },
    // {
    //   path: "/receta",
    //   component: Recepie,
    //   name: "receta"
    // },
    {
      path: "/receta/:uuid",
      name: "recipe",
      component: Recipe,
      props: true
    }
  ]
}
];


export default new VueRouter({
  //mode: "history",
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0,
      y: 0
    }
      // selector: 'start-app'}
  }
});