import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    stage: 1,
    food_time: null,
    proteins: [],
    tools: [],
    grains: [],
    fruits: [],
    veggies: [],
    spice_level: 1,
    user_data: {},
    day_night: false,
    //serverPath: "http://localhost:8888/amazon-7-dias-intensidad/public/api",
    serverPath: "https://www.amazonpepper.com/amazon-7-dias-intensidad/api",
    //serverPath: "https://www.amazonpepper.com/amazon-7-dias-intensidad/api",

    header: "prepara tu paladar para vivir esta intensidad...",
    step_bar: "¿Qué vas a preparar?",

    myRecipes: []
  },

  getters: {
    getStage: state => state.stage,
    getProteins: state => state.proteins,
    getTools: state => state.tools,
    getGrains: state => state.grains,
    getFruits: state => state.fruits,
    getVeggies: state => state.veggies,
    getSpiceLevel: state => state.spice_level,
    getUserData: state => state.user_data,
    getDayNight: state => state.day_night,
    getServerPath: state => state.serverPath,
    getHeader: state => state.header,
    getBar: state => state.step_bar,
    getMyRecipes: state => state.myRecipes
  },
  mutations: {
    SET_STAGE: (state, payload) => {
      state.stage = payload;
    },
    SET_TOOLS: (state, payload) => {
      state.tools = payload;
    },
    SET_PROTEINS: (state, payload) => {
      state.proteins = payload;
    },
    SET_GRAINS: (state, payload) => {
      state.grains = payload;
    },
    SET_FRUITS: (state, payload) => {
      state.fruits = payload;
    },
    SET_VEGGIES: (state, payload) => {
      state.veggies = payload;
    },
    SET_SPICE_LEVEL: (state, payload) => {
      state.spice_level = payload;
    },
    SET_USER_DATA: (state, payload) => {
      state.user_data = payload;
    },
    SET_DAY_NIGHT: (state, payload) => {
      state.day_night = payload;
    },
    SET_HEADER: (state, payload) => {
      state.header = payload;
    },
    SET_BAR: (state, payload) => {
      state.step_bar = payload;
    },
    SET_MY_RECIPES: (state, payload) => {
      state.myRecipes = payload;
    }
  },
  actions: {
    updateStage: (context, payload) => {
      context.commit("SET_STAGE", payload);
    },
    setTools: (context, payload) => {
      context.commit("SET_TOOLS", payload);
    },
    setProteins: (context, payload) => {
      context.commit("SET_PROTEINS", payload);
    },
    setGrains: (context, payload) => {
      context.commit("SET_GRAINS", payload);
    },
    setFruits: (context, payload) => {
      context.commit("SET_FRUITS", payload);
    },
    setSpiceLevel: (context, payload) => {
      context.commit("SET_SPICE_LEVEL", payload);
    },
    setVeggies: (context, payload) => {
      context.commit("SET_VEGGIES", payload);
    },
    setUserData: (context, payload) => {
      context.commit("SET_USER_DATA", payload);
    },
    setDayNight: (context, payload) => {
      context.commit("SET_DAY_NIGHT", payload);
    },
    setHeader: (context, payload) => {
      context.commit("SET_HEADER", payload);
    },
    setBar: (context, payload) => {
      context.commit("SET_BAR", payload);
    },
    setMyRecipes: (context, payload) => {
      context.commit("SET_MY_RECIPES", payload);
    }
  }
});
