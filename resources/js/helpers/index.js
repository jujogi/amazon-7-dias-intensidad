import axios from "axios";

export function getIngredients(store, category) {
  return axios
    .get(`${store.getters.getServerPath}/ingredients/${category}`)
    .then(response => {
      return response.data.category.filter_ingredients;
    })
    .catch(err => console.log(err));
}

export function removeSubjectIngredient(str) {
  let tmp = str.split(" ");
  return tmp.length > 1 ? tmp.slice(1).join(" ") : tmp.join(" ");
}

export function createMyRecipes(store) {
  return axios
    .get(`${store.getters.getServerPath}/selected/ingredients`, {
      params: {
        tools: store.getters.getTools.join(),
        proteins: store.getters.getProteins.join(),
        grains: store.getters.getGrains.join(),
        vegetables: store.getters.getVeggies.join(),
        fruits: store.getters.getFruits.join(),
        spicy: store.getters.getSpiceLevel,
        name: store.getters.getUserData.name,
        last_name: store.getters.getUserData.last_name,
        email: store.getters.getUserData.email
      }
    })
    .then(response => {
      return response.data.recipes;
    })
    .catch(err => console.log(err));
}

export function addScoreToIngredient(store, id) {
  axios.get(`${store.getters.getServerPath}/score/sum/ingredient/${id}`).then(response => {
  });
}
