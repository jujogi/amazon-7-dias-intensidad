
// const ID_TAG_MANAGER = "UA-153926121-1";
const ID_TAG_MANAGER = "UA-92960113-1";

export const sendGAEvent = (type, props) => {
    gtag(type, ID_TAG_MANAGER, props);
}
