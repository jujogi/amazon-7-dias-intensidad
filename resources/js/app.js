/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import Toasted from "vue-toasted";
import router from "./routes";
import store from "./storage";
import {sendGAEvent} from './helpers/analytics.js';


Vue.use(Toasted);


router.beforeEach((to, from, next) => {

  sendGAEvent('config', {
      'page_title' : to.name,
      'page_path': to.path
  })

  next();
})


const app = new Vue({
  el: "#app",
  router,
  store
});
