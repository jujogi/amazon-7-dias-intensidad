<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-153926121-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-92960113-1');
</script>

<link rel="icon" href="https://www.amazonpepper.com/wp-content/uploads/2019/08/cropped-FAV-1-32x32.png" sizes="32x32">
<link rel="icon" href="https://www.amazonpepper.com/wp-content/uploads/2019/08/cropped-FAV-1-192x192.png" sizes="192x192">
<link rel="apple-touch-icon-precomposed" href="https://www.amazonpepper.com/wp-content/uploads/2019/08/cropped-FAV-1-180x180.png">


    <meta charset="utf-8">
    <meta name="viewport" content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Amazon 7 Días de Intensidad</title>

<link rel="stylesheet" href="{{ asset('css/fonts.css?090') }}">
<link href="{{ asset('css/app.css?090') }}" rel="stylesheet">


    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    
</head>

<body>

    <div id="app">

      <router-view :key="$route.fullPath"></router-view>

    </div>

</body>


    <!-- <script type='text/javascript'>
      window.__lo_site_id = 184213;
      
        (function() {
          var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
          wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
          })();
        </script> -->
        
<script src="{{ asset('js/app.js?090') }}"></script>
</html>