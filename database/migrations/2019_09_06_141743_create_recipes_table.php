<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->text('uuid')->nullable();

            $table->text('set')->nullable();
            

            $table->integer('proteina_id')->nullable();
            $table->integer('fruta_id')->nullable();
            $table->integer('verdura_id')->nullable();
            $table->integer('salsa_id')->nullable();
            $table->integer('aderezo_id')->nullable();
            $table->integer('hortaliza_id')->nullable();



            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
