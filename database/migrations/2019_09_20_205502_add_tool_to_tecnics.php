<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToolToTecnics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tecnics', function (Blueprint $table) {
            //$table->string('tool')->nullable()->after('description');
            $table->text('protein')->nullable()->after('description');
            $table->text('grain')->nullable()->after('description');
            $table->text('fruit')->nullable()->after('description');
            $table->text('vegetable')->nullable()->after('description');
            $table->text('salad')->nullable()->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tecnics', function (Blueprint $table) {
           // $table->dropColumn('tool');
            $table->dropColumn('protein');
            $table->dropColumn('grain');
            $table->dropColumn('fruit');
            $table->dropColumn('vegetable');
            $table->dropColumn('salad');
        });
    }
}
