<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $families = [
            'Aves',
            'Fruta',
            'Verdura',
            'Cereal',
            'Tubérculo',
            'Mariscos',
            'Pescado',
            'Cuadrupedo'
        ];

        $categories = [
            'Proteína',
            'Frutas',
            'Verdura',
            'Guarnición'
        ];

        $tecnicas = [
            ["Asado", "Colocar una parrilla en una estufa a fuego alto y calentar, Con una brocha de cocina untar aceite o mantequilla sobre la parrilla, Colocar los ingredientes sobre la parrilla y dejar cocinar por 5 minutos, dar la vuelta a los ingredientes y dejar cocinar segun el punto que se quiero"],
            ["Barbacoa", "Colocar una parrilla en una estufa a fuego alto y calentar, Con una brocha de cocina untar aceite o mantequilla sobre la parrilla, Colocar los ingredientes sobre la parrilla y dejar cocinar por 5 minutos, dar la vuelta a los ingredientes y bajar la temperatura del fuego a bajo, mientras se logra el punto de coccion que se desee bañar con salsa para humedeser y dar sabor"],
            ["Braseado", "Colocar una olla de paredes altas que tenga tapa en una estufa a fuego alto, Agregar un poco de aceite junto con todos los ingredientes solidos y remover por 30 segundos, Agregar los ingredientes liquidos y bajar el fuego de la estufa a minimo, Tapar y dejar cocinar por 90 minutos"],
            ["Cazuela", "Colocar una cazuela con tapa en una estufa a fuego medio, Agregar un poco de aceite junto con todos los ingredientes solidos y remover por 30 segundos, Agregar los ingredientes liquidos y bajar el fuego de la estufa a minimo, Tapar y dejar cocinar por 90 minutos"],
            ["Escalfado", "Colocar una olla de paredes altas llena de agua en una estufa a fuego alto, Cuando el agua hierva por completo agregar los ingredientes al agua y revolver, Contabilizar 50 segundos de coccion y colar todo el contendido de la olla"],
            ["Frito", "Colocar una olla de paredes altas con aceite a la mitad en una estufa a fuego alto, Cuando el aceite se caliente por completobajar el fuego a medio y agregar los ingredientes, Contabilizar 8 minutos de coccion y colar todo el contendido del la olla"]
        ];


        foreach ($tecnicas as $key => $tecnica) {
            
            factory(App\Tecnic::class)->create([
                'name' => $tecnica[0],
                'description' => $tecnica[1]
            ]);
            
        }

        foreach ($categories as $key => $category) {
            
            factory(App\Category::class)->create([
                'name' => $category,
            ]);
            
        }

        foreach ($families as $key => $family) {
            
            factory(App\Family::class)->create([
                'name' => $family,
            ]);
            
        }





    }
}
