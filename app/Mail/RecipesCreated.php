<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecipesCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $set;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $set)
    {
        $this->user = $user;
        $this->set = $set;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Conoce tus 7 días de intensidad")
        ->from('noreply@amazonpepper.co', 'Amazon Pepper')
                    ->view('emails.recipesCreated');
    }
}
