<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $guarded = [];
    
    public function family()
    {
        return $this->hasOne('App\Family', 'id', 'family_id');
    }

    public function subfamily()
    {
        return $this->hasOne('App\Subfamily', 'id', 'subfamily_id');
    }

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function tecnics(){
        return $this->belongsToMany('App\Tecnic','ingredient_tecnics');
    }
    
    
}
