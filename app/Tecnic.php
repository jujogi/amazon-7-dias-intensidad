<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tecnic extends Model
{
    protected $guarded = [];

    public function ingredients(){
        return $this->belongsToMany('App\Ingredient','ingredient_tecnics');
    }

    public function tools(){
        return $this->belongsToMany('App\Tool','tecnic_tools');
    }
}
