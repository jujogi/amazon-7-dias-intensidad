<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{

    public function show(){

        $categories = Category::all();

        return response()->json([
            'categories' => $categories
        ], 200);

    }

    public function store(Request $request){

        $category = Category::create([
            'name' => $request->input('name')
        ]);

        return response()->json([
            'category' => $category,
            'message' => 'Categoría añadida correctamente'
        ], 200);


    }

    public function count(){

        $categories = Category::withCount(['ingredients'])->get();
        return $categories;
    }
}
