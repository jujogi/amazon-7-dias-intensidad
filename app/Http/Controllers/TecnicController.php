<?php

namespace App\Http\Controllers;
use App\Tecnic;
use Illuminate\Http\Request;


class TecnicController extends Controller
{

    public function show(){

        // $tecnics = Tecnic::all();
        // foreach ($tecnics as $key => $tecnic) {
        //     dd($tecnic->ingredients);
        // }
        $tecnics = Tecnic::with('ingredients')->get();

        return response()->json([
            'tecnics' => $tecnics->load('tools')
        ], 200);

    }

    public function store(Request $request){

        $tools = $request->input('tools');

        $tecnic = Tecnic::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'salad' => $request->input('salad'),
            'vegetable' => $request->input('vegetable'),
            'fruit' => $request->input('fruit'),
            'grain' => $request->input('grain'),
            'cereal' => $request->input('cereal'),
            'protein' => $request->input('protein')
        ]);

        $tecnic->tools()->attach($tools);


        return response()->json([
            'tecnic' => $tecnic,
            'message' => 'Técnica de cocción añadida correctamente'
        ], 200);

    }

    public function edit(Request $request)
    {

        
        $tecnic = Tecnic::find($request->input('tecnic_id'));
        $tools = $request->input('tools');

        //return $tools;

        $tecnic->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'salad' => $request->input('salad'),
            'vegetable' => $request->input('vegetable'),
            'fruit' => $request->input('fruit'),
            'grain' => $request->input('grain'),
            'cereal' => $request->input('cereal'),
            'protein' => $request->input('protein')
        ]);

        $tecnic->tools()->detach();
        $tecnic->tools()->attach($tools);



        return response()->json([
            'tecnic' => $tecnic,
            'message' => 'Técnica editada correctamente'
        ], 200);

    }

}
