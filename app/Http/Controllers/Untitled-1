<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;
use App\Recipe;
use Illuminate\Support\Str;


class IngredientController extends Controller
{

    public function generateRecipe($excludeArrays)
    {

        $recipes = [
            'proteina' => 1,
            'fruta' => 2,
            'verdura' => 3,
            'salsa' => 5,
            'aderezo' => 6,
            'hortaliza' => 7
        ];

        $recipe = array();

        //Escojo la proteina y a partir de ella busco las técnicas de cocción para los demás ingredientes
        $proteina = Ingredient::where('category_id', $recipes['proteina'])->whereHas('tecnics', function ($query) {
            return $query;
       })->inRandomOrder()->first();

        $proteina->tecnic = $this->getRandomTecnic($proteina);
        $tecnic =  !is_null($proteina->tecnic) ? $proteina->tecnic : null;
        
        foreach ($recipes as $key => $category) {

            $ingredient = Ingredient::where('category_id', $category)->whereNotIn('category_id', $excludeArrays)->whereHas('tecnics', function ($query) use ($tecnic){
                 return $query
                 ->where('tecnics.id', $tecnic);
            })->inRandomOrder()->first();

            if ($ingredient) {
                $ingredient->tecnic = $this->getRandomTecnic($ingredient, $tecnic);
                $ingredient->load('family', 'category');

                array_push($recipe, $ingredient);
            } else {

                //¿Que pasa si el sistema no encuentra ingredientes acordes?
                //Traiga aleatoriamente un ingrediente!

                $ingredient = Ingredient::where('category_id', $category)->whereHas('tecnics', function ($query) {
                    return $query
                    ->where('tecnics.id', 1);
               })->inRandomOrder()->first();

                //dd($ingredient);
                $ingredient->load('family', 'category');

                array_push($recipe, $ingredient);
            }
        }

        return [
            'recipe' => $recipe,
            'exclude' => array_column($recipe, 'id')
        ];

    }

    public function getRandomTecnic(Ingredient $ingredient, $tecnic = null)
    {   
        if(!is_null($tecnic)){
            return $ingredient->tecnics()->where('tecnics.id', $tecnic)->first();
        } else {
            return $ingredient->tecnics()->inRandomOrder()->first();
        }
        
    }


    public function recipes()
    {

        $exclude = array();
        $results = array();
        $days = 1;
        $recipes = array();
        $set = Str::random(10);

        while ($days <= 7) {
            $recipe =  $this->generateRecipe($results);
            $exclude[] = $recipe['exclude'];
            $results = array_merge(...$exclude);
            $savedRecipe = $this->saveRecipe($recipe['exclude'], 1, $set);
            $recipe['recipe']['uuid'] = $savedRecipe->uuid;
            array_push($recipes, $recipe['recipe']);
            $days++;
        }

        return response()->json([
            'recipes' => $recipes,
        ], 200);
    }

    public function recipe($uuid)
    {

        $recipe = Recipe::where('uuid', $uuid)->first();

        $Allrecipe = [
            Ingredient::where('id', $recipe->proteina_id)->first(),
            Ingredient::where('id', $recipe->fruta_id)->first(),
            Ingredient::where('id', $recipe->verdura_id)->first(),
            Ingredient::where('id', $recipe->salsa_id)->first(),
            Ingredient::where('id', $recipe->aderezo_id)->first(),
            Ingredient::where('id', $recipe->hortaliza_id)->first()
        ];

        return response()->json([
            'recipe' => $Allrecipe,
        ], 200);
    }

    public function saveRecipe($ids, $user_id, $set)
    {



       $recipe = Recipe::create([
            'user_id' => $user_id,
            'proteina_id' => $ids[0],
            'fruta_id' => $ids[1],
            'verdura_id' => $ids[2],
            'salsa_id' => $ids[3],
            'aderezo_id' => $ids[4],
            'hortaliza_id' => $ids[5],
            'set' => $set,
            'uuid' => Str::random(10)
        ]);
        return $recipe;
    }



    public function show()
    {
        $ingredients = Ingredient::all();

        return response()->json([
            'ingredients' => $ingredients->load('family', 'category', 'tecnics')
        ], 200);
    }


    public function store(Request $request)
    {

        $tecnics = $request->input('tecnics');

        $ingredient = Ingredient::create([
            'name' => $request->input('name'),
            'family_id' => $request->input('family'),
            'category_id' => $request->input('category'),
            'breakfast' => $request->input('breakfast'),
            'lunch' => $request->input('lunch'),
            'dinner' => $request->input('dinner'),
            'serve' => $request->input('serve')
        ]);

        $ingredient->tecnics()->attach($tecnics);


        return response()->json([
            'ingredient' => $ingredient->load('family', 'category', 'tecnics'),
            'message' => 'Ingrediente añadido correctamente'
        ], 200);
    }

    public function addTecnics(Ingredient $ingredient)
    {
        return $ingredient->tecnics;
        $ingredient->tecnics()->attach(1);
    }

    public function edit(Request $request)
    {
        $ingredient = Ingredient::find($request->input('ingredient_id'));
        $tecnics = $request->input('tecnics');

        $ingredient->update([
            'name' => $request->input('name'),
            'family_id' => $request->input('family'),
            'category_id' => $request->input('category'),
            'breakfast' => $request->input('breakfast'),
            'lunch' => $request->input('lunch'),
            'dinner' => $request->input('dinner'),
            'serve' => $request->input('serve')
        ]);
        $ingredient->tecnics()->detach();
        $ingredient->tecnics()->attach($tecnics);

        return response()->json([
            'ingredient' => $ingredient->load('family', 'category', 'tecnics'),
            'message' => 'Ingrediente editado correctamente'
        ], 200);
    }
}
