<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;
use App\Recipe;
use App\Category;
use App\Tecnic;
use App\Tool;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Mail;
use App\Mail\RecipesCreated;


class IngredientController extends Controller
{   

    public function favorites(){

        $proteins = Ingredient::where('category_id', 1)->orderBy('score', 'desc')->take(3)->get(['name', 'image', 'score']);

        $grains = Ingredient::where('category_id', 9)->orderBy('score', 'desc')->take(1)->get(['name', 'image', 'score']);

        $vegetables = Ingredient::where('category_id', 3)->orderBy('score', 'desc')->take(1)->get(['name', 'image', 'score']);

        $fruits = Ingredient::where('category_id', 2)->orderBy('score', 'desc')->take(1)->get(['name', 'image', 'score']);


        $ingredients = [
            $proteins[0],
            $proteins[1],
            $proteins[2],
            $grains[0],
            $vegetables[0],
            $fruits[0],
        ];

        
        return response()->json([
            'ingredients' => $ingredients            
        ], 200);

    }


    public function users($pass){

        if($pass == "owak"){
            $users = User::all();

            return response()->json([
                'users' => $users,
                'length' => $users->count()
                
            ], 200);
        }
    }

    public function recipes($set)
    {

        $recipes = Recipe::where('set', $set)->get();
        $newRecipes = array();

        foreach ($recipes as $key => $recipe) {

            $recipe = [
                'tecnic' => Tecnic::where('id', $recipe->cook_id)->first()->toArray(),
                'protein' => Ingredient::where('id', $recipe->proteina_id)->first()->toArray(),
                'grain' => Ingredient::where('id', $recipe->grano_id)->first()->toArray(),
                'vegetable' => Ingredient::where('id', $recipe->verdura_id)->first()->toArray(),
                'fruit' => Ingredient::where('id', $recipe->fruta_id)->first()->toArray(),
                'hortaliza' => Ingredient::where('id', $recipe->hortaliza_id)->first()->toArray(),
                'aderezo' => Ingredient::where('id', $recipe->aderezo_id)->first()->toArray(),
                'salsa' => Ingredient::where('id', $recipe->salsa_id)->first()->toArray(),
                'especia' => Ingredient::where('id', $recipe->especia_id)->first()->toArray(),
                'uuid' => $recipe->uuid,
                'set' => $recipe->set,
                'user_id' => $recipe->user_id
            ];
            array_push($newRecipes, $recipe);
        }


        return response()->json([
            'recipes' => $newRecipes,
            'user' => User::find($newRecipes[0]['user_id'])
        ], 200);
    }

    public function recipe($uuid)
    {

        $recipe = Recipe::where('uuid', $uuid)->first();

        $tecnic = Tecnic::where('id', $recipe->cook_id)->first();

        $protein = Ingredient::where('id', $recipe->proteina_id)->first();



        $fruta = Ingredient::where('id', $recipe->fruta_id)->first();

        $grano = Ingredient::where('id', $recipe->grano_id)->first();

        $cereal = Ingredient::where('id', $recipe->cereal_id)->first();

        $verdura = Ingredient::where('id', $recipe->verdura_id)->first();
        $salsa = Ingredient::where('id', $recipe->salsa_id)->first();
        $aderezo = Ingredient::where('id', $recipe->aderezo_id)->first();
        $hortaliza = Ingredient::where('id', $recipe->hortaliza_id)->first();
        $especia = Ingredient::where('id', $recipe->especia_id)->first();

        $ingredients = [
            $protein,
            $grano,
            $cereal,
            $fruta,
            $verdura,
            $salsa,
            $aderezo,
            $hortaliza,
            $especia,
        ];

        return response()->json([
            'tecnic' => $tecnic,
            'ingredients' => $ingredients,
            'set' => $recipe->set
        ], 200);
    }


    public function sendRecipesToUser($user, $set)
    {
        Mail::to($user->email)
            ->send(new RecipesCreated($user, $set));
    }

    public function getRandomTecnic(Ingredient $ingredient)
    {
        return $ingredient->tecnics()->inRandomOrder()->first();
    }

    public function show()
    {
        $ingredients = Ingredient::orderBy('category_id', 'asc')->get();

        return response()->json([
            'ingredients' => $ingredients->load('family', 'category', 'tecnics', 'subfamily')
        ], 200);
    }


    public function store(Request $request)
    {

        $tecnics = $request->input('tecnics');

        $ingredient = Ingredient::create([
            'name' => $request->input('name'),
            'family_id' => $request->input('family'),
            'subfamily_id' => $request->input('subfamily'),
            'category_id' => $request->input('category'),
            'breakfast' => $request->input('breakfast'),
            'lunch' => $request->input('lunch'),
            'dinner' => $request->input('dinner'),
            'serve' => $request->input('serve')
        ]);

        $ingredient->tecnics()->attach($tecnics);


        return response()->json([
            'ingredient' => $ingredient->load('family', 'category', 'tecnics'),
            'message' => 'Ingrediente añadido correctamente'
        ], 200);
    }

    public function addTecnics(Ingredient $ingredient)
    {
        return $ingredient->tecnics;
        $ingredient->tecnics()->attach(1);
    }

    public function edit(Request $request)
    {
        $ingredient = Ingredient::find($request->input('ingredient_id'));
        $tecnics = $request->input('tecnics');

        $ingredient->update([
            'name' => $request->input('name'),
            'family_id' => $request->input('family'),
            'subfamily_id' => $request->input('subfamily'),
            'category_id' => $request->input('category'),
            'breakfast' => $request->input('breakfast'),
            'lunch' => $request->input('lunch'),
            'dinner' => $request->input('dinner'),
            'serve' => $request->input('serve'),
            'image' => $request->input('image')
        ]);
        $ingredient->tecnics()->detach();
        $ingredient->tecnics()->attach($tecnics);

        return response()->json([
            'ingredient' => $ingredient->load('family', 'category', 'tecnics'),
            'message' => 'Ingrediente editado correctamente'
        ], 200);
    }


    public function delete(Ingredient $ingredient)
    {

        $ingredient->tecnics()->detach();
        $ingredient->delete();
    }


    //Muestra los ingredientes de una categoría específica
    public function showIngredients($category)
    {

        $category = Category::where('id', $category)->first()->load('filterIngredients');

        return response()->json([
            'category' => $category
        ], 200);
    }

    public function showProteinsByTecnics(Request $request)
    {


        $selectedTools = explode(',', $request->input('tools'));
        $toolsIds = Tool::whereIn('parent', $selectedTools)->pluck('id')->toArray();

        $callback = function ($query) {
            $query->where('ingredients.category_id', 1);
        };

        $proteins =  Ingredient::whereHas('tecnics', function ($query) use ($toolsIds) {
            $query->whereIn('tecnics.id', $toolsIds);
        })->where('category_id', 1)->orderBy('score', 'desc')->get();


        return response()->json([
            'proteins' => $proteins
        ], 200);

        //return $proteins;
        //dd($proteins);

        // dd($input);
    }

    //Suma +1 por cada selección.
    public function sumScore(Ingredient $ingredient)
    {
        //return $ingredient;
        $ingredient->score = $ingredient->score + 1;
        $ingredient->save();

        return response()->json([
            'message' => 'Se ha añadido valoración en el ingrediente: ' . $ingredient->score
        ], 200);
    }


    public function createUser($user)
    {


        $user = User::firstOrCreate(
            ['email' => $user['email']],
            [
                'name' => $user['name'],
                'last_name' => $user['last_name'],
                'password' => Hash::make(str_random(8))
            ]
        );

        return $user;
    }


    public function obtainIngredients(Request $request)
    {

        $userArray = [
            'name' => $request->input('name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
        ];

        $user = $this->createUser($userArray);


        $cereales = [57, 58, 173, 174, 175, 176];

        $selectedTools = explode(',', $request->input('tools'));
        $toolsIds = Tool::whereIn('parent', $selectedTools)->pluck('id')->toArray();

        $selectedProteins = explode(',', $request->input('proteins'));
        $selectedGrains = explode(',', $request->input('grains'));
        $selectedVegetables = explode(',', $request->input('vegetables'));
        $selectedFruits = explode(',', $request->input('fruits'));
        $selectedLevelSpicy = ($request->input('spicy')) ? $request->input('spicy') : 1;


        //VALIDAR SI EN LOS GRANOS HAY CEREALES...

        //Obtener los cereales de los vegetales seleccionados
        $selectedCereals = array_intersect($selectedGrains, $cereales);

        //Quitar de los vegetales, los cereales
        $filteredGrains = array_diff($selectedGrains, $cereales);

       

        //dd($selectedCereals);



        $callback = function ($query) use ($selectedProteins) {
            $query->whereIn('ingredients.id', $selectedProteins);
        };

        $tecnics =  Tecnic::whereHas('tools', function ($query) use ($toolsIds) {
            $query->whereIn('tools.id', $toolsIds);
        })->get();

        $tecnicsIds = $tecnics->pluck('id');
        //dd($tecnicsIds);

        $proteins = $this->duplicateCollection(Ingredient::whereIn('id', $selectedProteins)->get());

        $filteredGrains = (!empty($filteredGrains)) ? $filteredGrains : [184, 183, 182];

        $grains = Ingredient::whereIn('id', $filteredGrains)->get()->toArray();

        $vegetables = Ingredient::whereIn('id', $selectedVegetables)->get()->toArray();
        $fruits = Ingredient::whereIn('id', $selectedFruits)->get()->toArray();
        $cereals = Ingredient::whereIn('id', $selectedCereals)->get()->toArray();


        //dd($proteins);
        $structure = array();

        foreach ($proteins as $key => $protein) {

            $tempProtein = $protein;
            $tempTecnic = $protein->tecnics()->whereIn('tecnic_id', $tecnicsIds)->inRandomOrder()->first();

            $temp = [
                'tecnic' => $tempTecnic->toArray(),
                'protein' => $tempProtein->toArray()
            ];

            array_push($structure, $temp);
        }


        //Enviar los granos, vegetales, y frutas
        $recipes = $this->createCustomRecipe($structure, $grains, $cereals, $vegetables, $fruits, $selectedLevelSpicy, $user);


        return response()->json([
            'recipes' => $recipes
        ], 200);


        //dd($recipes);
    }

    public function createCustomRecipe($structure, $grains, $cereals, $vegetables, $fruits, $spicy, $user)
    {

        $recipes = array();
        $arraySpicy = array();
        
        //Debe tomar solo la salsa del picante que seleccionó, por eso se oculta esta función.

        if($spicy == 3){ //Si selecciona nivel alto
            $arraySpicy = [2,3]; //Coge alto y medio
        }
        if($spicy == 2) { //Si selecciona medio
            $arraySpicy = [2]; //Coge medio
        }
        if($spicy == 1){ //Si selecciona bajo
            $arraySpicy = [1]; //Solo muestra bajo
        }

        //Mínimo debe tener 7, sino, repite una de las que eligió.
        $grains = $this->autoCompleteIngredient($grains);
        $cereals = (!empty($cereals)) ? $this->autoCompleteIngredient($cereals) : null;
        $vegetables = $this->autoCompleteIngredient($vegetables);
        $fruits = $this->autoCompleteIngredient($fruits);

        $set = Str::random(10);

       // dd($vegetables);

        $recipesCount = 0;

        while ($recipesCount < 10) {


            $tecnic = $structure[$recipesCount]['tecnic'];
            $protein = $structure[$recipesCount]['protein'];
            $grain = $grains[$recipesCount];
            $cereal = (!is_null($cereals)) ? $cereals[$recipesCount] : null;
            $vegetable = $vegetables[$recipesCount];
            $fruit = $fruits[$recipesCount];


            $recipe = [
                'tecnic' => $tecnic,
                'protein' => $protein,
                'grain' => $grain,
                'cereal' => $cereal,
                'vegetable' => $vegetable,
                'fruit' => $fruit,
                'hortaliza' => Ingredient::where('category_id', 7)->inRandomOrder()->first()->toArray(),
                'aderezo' => Ingredient::where('category_id', 6)->inRandomOrder()->first()->toArray(),
                'salsa' => Ingredient::where('category_id', 5)->whereIn('spicy', $arraySpicy)->inRandomOrder()->first()->toArray(),
                'especia' => Ingredient::where('category_id', 11)->inRandomOrder()->first()->toArray(),
                'set' => $set
            ];


            //dd($recipe);

            $newRecipe = $this->saveRecipe($recipe, $set, $user);
            $recipe['uuid'] = $newRecipe->uuid;


            array_push($recipes, $recipe);
            $recipesCount++;
        }

        //Correo:
        $this->sendRecipesToUser($user, $set);

        return $recipes;
    }

    public function saveRecipe($recipe, $set, $user)
    {


        //Recetas vinculadas al usuario...

        //$user_id = 1;
        //dd($recipe);

        $newRecipe = Recipe::create([
            'user_id' => $user->id,
            'cook_id' => $recipe['tecnic']['id'],
            'proteina_id' => $recipe['protein']['id'],
            'fruta_id' => $recipe['fruit']['id'],
            'grano_id' => $recipe['grain']['id'],
            'cereal_id' => (!is_null($recipe['cereal'])) ? $recipe['cereal']['id'] : null,
            'verdura_id' => $recipe['vegetable']['id'],
            'salsa_id' => $recipe['salsa']['id'],
            'aderezo_id' => $recipe['aderezo']['id'],
            'hortaliza_id' => $recipe['hortaliza']['id'],
            'especia_id' => $recipe['especia']['id'],
            'set' => $set,
            'uuid' => Str::random(10)
        ]);


        //dd($newRecipe);

        return $newRecipe;
    }

    public function set7Tecnics($arr)
    {

        $size = sizeof($arr);

        if ($size > 10) {
            $diff = $size - 10;
            //dd($diff);
            array_splice($arr, $size - $diff, $diff);
            //array_slice($arr, 0, $diff);
            return $arr;
        } else {
            return $arr;
        }
    }


    public function duplicateCollection($col)
    {

        $size = $col->count();


        if ($size < 10) {

            while ($size < 10) {

                $rand = rand(0, $size - 1);


                $col->push($col[$rand]);
                $size = $size + 1;
            }
        } else {
            return $col;
        }

        return $col;

        //dd($col);

    }


    public function autoCompleteIngredient($arr)
    {
        //dd($arr);
        $size = sizeof($arr);
        //dd($size);

        if ($size < 10) {

            while ($size < 10) {

                $arr[$size] = $arr[array_rand($arr)];
                $size = $size + 1;
            }
        } else {
            return $arr;
        }

        //dd($arr);
        return $arr;
    }
}
