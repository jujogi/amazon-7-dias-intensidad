<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subfamily;
use App\Ingredient;

class SubfamilyController extends Controller
{

    public function store(Request $request)
    {

        $family = Subfamily::create([
            'name' => $request->input('name'),
            'family_id' => $request->input('family')
        ]);

        return response()->json([
            'family' => $family,
            'message' => 'Subfamilia añadida correctamente'
        ], 200);
    }

    public function edit(Subfamily $subfamily, Request $request){
        //return $subfamily;
        $name = $request->input('name');
        $subfamily->update([
            'name' => $name
        ]);



        //return "Ajuste hecho";
    }

    public function delete(Subfamily $subfamily){
        //return $subfamily;
        $ingredients = Ingredient::where('subfamily_id', $subfamily->id)->get();
        //return $ingredients;

        if($ingredients->count() > 0){
        foreach ($ingredients as $key => $ingredient) {
            $ingredient->subfamily_id = null;
            $ingredient->save();
        }
    }

    $subfamily->delete();
        //return $subfamily;
        //$name = $request->input('name');
        // $subfamily->update([
        //     'name' => $name
        // ]);

        

        //return "Ajuste hecho";
    }


}
