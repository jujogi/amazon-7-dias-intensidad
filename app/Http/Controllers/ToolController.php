<?php

namespace App\Http\Controllers;

use App\Tool;
use Illuminate\Http\Request;

class ToolController extends Controller
{
    public function show(){

        // $tecnics = Tecnic::all();
        // foreach ($tecnics as $key => $tecnic) {
        //     dd($tecnic->ingredients);
        // }
        $tools = Tool::all();

        return response()->json([
            'tools' => $tools
        ], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request){

        $tool = Tool::create([
            'name' => $request->input('name')
        ]);

        return response()->json([
            'tool' => $tool,
            'message' => 'Herramienta añadida correctamente'
        ], 200);

    }


    public function edit(Request $request)
    {
        $tool = Tool::find($request->input('tool_id'));

        $tool->update([
            'name' => $request->input('name')
        ]);

        return response()->json([
            'tool' => $tool,
            'message' => 'Herramienta editada correctamente'
        ], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tool $tool)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tool $tool)
    {
        //
    }


    public function getTools(){
        
        $tools = Tool::whereHas('tecnics', function($q){
            $q->whereHas('ingredients');
        })->get();
        

        return response()->json([
            'tools' => $tools
        ], 200);


    }
}
