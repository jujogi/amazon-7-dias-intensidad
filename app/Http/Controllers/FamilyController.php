<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;
use App\Ingredient;

class FamilyController extends Controller
{


    public function show()
    {
        $families = Family::all();

        return response()->json([
            'families' => $families->load('subfamilies')
        ], 200);
    }


    public function family(Family $family)
    {
        $sub = $family->load('subfamilies');

        return response()->json([
            'subfamilies' => $sub->subfamilies
        ], 200);
    }

    public function store(Request $request)
    {

        $family = Family::create([
            'name' => $request->input('name')
        ]);

        return response()->json([
            'family' => $family,
            'message' => 'Familia añadida correctamente'
        ], 200);
    }

    public function edit(Request $request)
    {
        
        $family = Family::find($request->input('family_id'));

        $family->update([
            'name' => $request->input('name'),
        ]);

        return response()->json([
            'family' => $family,
            'message' => 'Familia editada correctamente'
        ], 200);
    }


    public function delete(Family $family){

       $ingredients = Ingredient::where('family_id', $family->id)->get();

        if($ingredients->count() > 0){
            foreach ($ingredients as $ingredient) {
                $ingredient->family_id = null;
                $ingredient->subfamily_id = null;
                $ingredient->save();
            }
        }

       $family->subfamilies()->delete();
        $family->delete();



       // return $ingredients;

    }

}
