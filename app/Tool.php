<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    protected $guarded = [];

    public function tecnics(){
        return $this->belongsToMany('App\Tecnic','tecnic_tools');
    }

}
