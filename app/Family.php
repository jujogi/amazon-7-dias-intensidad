<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $guarded = [];

    public function subfamilies(){
        return $this->hasMany(Subfamily::class)->orderBy('created_at', 'desc');
    }

}
