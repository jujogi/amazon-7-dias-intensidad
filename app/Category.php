<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function ingredients(){
        return $this->hasMany(Ingredient::class)->orderBy('score', 'desc');
    }

    public function filterIngredients(){
        return $this->hasMany(Ingredient::class)->select(['id','name', 'category_id', 'score', 'image'])->orderBy('score', 'desc');
    }
    
}
