<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//API DASHBOARD
Route::get('ingredients', 'IngredientController@show');
Route::get('ingredient/{ingredient}', 'IngredientController@addTecnics');
Route::post('add/ingredient', 'IngredientController@store');
Route::delete('ingredient/delete/{ingredient}', 'IngredientController@delete');
Route::put('ingredient/edit', 'IngredientController@edit');


Route::post('add/category', 'CategoryController@store');
Route::get('categories', 'CategoryController@show');

Route::post('add/family', 'FamilyController@store');
Route::post('add/subfamily', 'SubfamilyController@store');
Route::get('subfamily/{family}', 'FamilyController@family');
Route::put('subfamily/{subfamily}/edit', 'SubfamilyController@edit');
Route::delete('subfamily/{subfamily}/delete', 'SubfamilyController@delete');
Route::get('families', 'FamilyController@show');

Route::delete('family/{family}/delete', 'FamilyController@delete');


Route::post('add/tool', 'ToolController@store');
Route::get('tools', 'ToolController@show');
Route::put('tool/edit', 'ToolController@edit');

Route::put('family/edit', 'FamilyController@edit');

Route::post('add/tecnic', 'TecnicController@store');
Route::put('tecnic/edit', 'TecnicController@edit');
Route::get('tecnics', 'TecnicController@show');


Route::post('add/ingredient', 'IngredientController@store');
Route::post('add/cook', 'IngredientController@store');

//Route::get('generate/recipes', 'IngredientController@recipes');

Route::get('recipe/{uuid}', 'IngredientController@recipe');

Route::get('users/{pass}', 'IngredientController@users');


Route::get('total/categories', 'CategoryController@count');


//API APP

Route::get('tools/filtered', 'ToolController@getTools');
Route::get('ingredients/{category}', 'IngredientController@showIngredients');
Route::get('proteins', 'IngredientController@showProteinsByTecnics');
Route::get('ingredients/tools', 'IngredientController@showTools');

Route::get('selected/ingredients', 'IngredientController@obtainIngredients');

Route::get('score/sum/ingredient/{ingredient}', 'IngredientController@sumScore');

Route::get('favorite/ingredients', 'IngredientController@favorites');


Route::get('generate/recipes/{set}', 'IngredientController@recipes');










